﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPL.Shape
{
    class Paralellogram : DrawingObject
    {
        public Point startPoint { get; set; }
        public Point finishPoint { get; set; }
        private Pen pen;
       
        public Paralellogram()
        {
            this.pen = new Pen(Color.Black);
        }

        public Paralellogram(Point initX) : this()
        {
            this.startPoint = initX;
        }

        public Paralellogram(Point initX, Point initY) : this(initX)
        {
            this.finishPoint = initY;
        }


        public override void DrawEdit()
        {
            pen.Color = Color.Blue;
            pen.DashStyle = DashStyle.Solid;
            Point point1 = new Point(startPoint.X + 50, startPoint.Y);
            Point point2 = new Point(finishPoint.X + 50, startPoint.Y);
            Point point3 = new Point(startPoint.X, finishPoint.Y);
            Point point4 = new Point(finishPoint.X, finishPoint.Y);
            Point[] titik = { point1, point2, point4, point3 };
            this.graphics.DrawPolygon(pen, titik);
            Notify();
        }

        public override bool HitArea(int x, int y)
        {
            if ((x >= startPoint.X && x <= startPoint.X + finishPoint.X) && (y >= startPoint.Y && y <= startPoint.Y + finishPoint.Y))
            {
                return true;
            }
            return false;
        }

        public override void DrawIdle()
        {
            pen.Color = Color.Black;
            pen.DashStyle = DashStyle.Solid;
            Point point1 = new Point(startPoint.X + 50, startPoint.Y);
            Point point2 = new Point(finishPoint.X + 50, startPoint.Y);
            Point point3 = new Point(startPoint.X, finishPoint.Y);
            Point point4 = new Point(finishPoint.X, finishPoint.Y);
            Point[] titik = { point1, point2, point4, point3 };
            this.graphics.DrawPolygon(pen, titik);
            Notify();
        }
        public override void DrawPreview()
        {
            pen.Color = Color.Blue;
            pen.DashStyle = DashStyle.DashDotDot;
            Point point1 = new Point(startPoint.X+50,startPoint.Y);
            Point point2 = new Point(finishPoint.X+50,startPoint.Y);
            Point point3 = new Point(startPoint.X,finishPoint.Y);
            Point point4 = new Point(finishPoint.X,finishPoint.Y);
            Point[] titik = { point1, point2, point4, point3 };
            this.graphics.DrawPolygon(pen, titik);
            Debug.WriteLine(point1 + "1");
            Debug.WriteLine(point2 + "2");
            Debug.WriteLine(point3 + "3");
            Debug.WriteLine(point4 + "4");
            Notify();
        }
        public override Point GetCenterPoint()
        {
            Point point = new Point();
            point.X = (finishPoint.X - startPoint.X) / 2 + startPoint.X;
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y;
            return point;
        }


        public override Point InLeft()
        {
            Point point = new Point();
            point.X = (finishPoint.X - startPoint.X) / 2 + startPoint.X-((finishPoint.X - startPoint.X) / 2);
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y;
            return point;
        }

        public override Point InRight()
        {
            Point point = new Point();
            point.X = (finishPoint.X - startPoint.X) / 2 + startPoint.X+ ((finishPoint.X - startPoint.X) / 2)+30;
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y;
            return point;
        }

        public override Point InBot()
        {
            Point point = new Point();
            point.X = (finishPoint.X - startPoint.X) / 2 + startPoint.X+ ((finishPoint.X - startPoint.X) / 4);
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y-((finishPoint.Y - startPoint.Y) / 2);
            return point;
        }

        public override Point FromRight()
        {
            Point point = new Point();
            point.X = (finishPoint.X - startPoint.X) / 2 + startPoint.X + ((finishPoint.Y - startPoint.Y))-10;
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y;
            return point;
        }

        public override Point FormLeft()
        {
            Point point = new Point();
            point.X = (finishPoint.X - startPoint.X) / 2 + startPoint.X - ((finishPoint.Y - startPoint.Y) / 2)+10;
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y;
            return point;
        }

        public override Point FromTop()
        {
            Point point = new Point();
            point.X = (finishPoint.X - startPoint.X) / 2 + startPoint.X + ((finishPoint.X - startPoint.X) / 4);
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y+ ((finishPoint.Y - startPoint.Y) / 2);
            return point;
        }
        public override string Type()
        {
            String nama = "Paralellogram";
            return nama;
        }

        public override void ChangeValue()
        {
            throw new NotImplementedException();
        }

        public override void Move(MouseEventArgs e, int x, int y)
        {
            this.startPoint = new Point((this.startPoint.X + x), (this.startPoint.Y + y));
            this.finishPoint = new Point((this.finishPoint.X + x), (this.finishPoint.Y + y));
        }
    }
}
