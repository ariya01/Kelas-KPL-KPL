﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPL.Shape
{
    public class Rectangle : DrawingObject
    {
        public int rectX { get; set; }
        public int rectY { get; set; }
        public int rectWidth { get; set; }
        public int rectHeight { get; set; }
        private Pen pen;

        public Rectangle()
        {
            this.pen = new Pen(Color.Black);
        }

        public Rectangle(int initX, int initY) : this()
        {
            this.rectX = initX;
            this.rectY = initY;
        }

        public Rectangle(int initX, int initY, int initWidth, int initHeight) : this(initX, initY)
        {
            this.rectHeight = initHeight;
            this.rectWidth = initWidth;
        }

        public override void DrawEdit()
        {
            pen.Color = Color.Blue;
            pen.DashStyle = DashStyle.Solid;
            this.graphics.DrawRectangle(this.pen, rectX, rectY, rectWidth, rectHeight);
            Debug.WriteLine(rectX + "is initialized.");
            Debug.WriteLine(rectY + "is initialized.");
            Debug.WriteLine(rectHeight + "is initialized.");
            Debug.WriteLine(rectWidth + "is initialized.");
            Notify();
        }

        public override bool HitArea(int x, int y)
        {
            if ((x >= rectX && x <= rectX + rectWidth) && (y >= rectY && y <= rectY + rectHeight))
            {
                return true;
            }
            return false;
        }

        public override void DrawIdle()
        {
            pen.Color = Color.Black;
            pen.DashStyle = DashStyle.Solid;
            this.graphics.DrawRectangle(this.pen, rectX, rectY, rectWidth, rectHeight);
            Notify();

        }

        public override void DrawPreview()
        {
            pen.Color = Color.Blue;
            pen.DashStyle = DashStyle.DashDotDot;
            this.graphics.DrawRectangle(this.pen, rectX, rectY, rectWidth, rectHeight);
            Notify();
        }

        public override Point GetCenterPoint()
        {
            Point point = new Point();
            point.X = rectX + (rectWidth / 2);
            point.Y = rectY + (rectHeight / 2);
            return point;
        }
        public override Point InLeft()
        {
            Point point = new Point();
            point.X = rectX;
            point.Y = rectY + (rectHeight / 2);
            return point;
        }

        public override Point InRight()
        {
            Point point = new Point();
            point.X = rectX + (rectWidth);
            point.Y = rectY + (rectHeight / 2);
            return point;
        }

        public override Point InBot()
        {
            Point point = new Point();
            point.X = rectX + (rectWidth / 2);
            point.Y = rectY;
            return point;
        }

        public override Point FromRight()
        {
            Point point = new Point();
            point.X = rectX + (rectWidth);
            point.Y = rectY + (rectHeight / 2);
            return point;
        }

        public override Point FormLeft()
        {
            Point point = new Point();
            point.X = rectX ;
            point.Y = rectY + (rectHeight / 2);
            return point;
        }

        public override Point FromTop()
        {
            Point point = new Point();
            point.X = rectX + (rectWidth / 2);
            point.Y = rectY + (rectHeight);
            return point;
        }

        public override string Type()
        {
            String nama = "Rectangle";
            return nama;
        }

        public override void ChangeValue()
        {
            throw new NotImplementedException();
        }

        public override void Move(MouseEventArgs e, int x, int y)
        {
            this.rectX += x;
            this.rectY += y;
        }
    }
}
