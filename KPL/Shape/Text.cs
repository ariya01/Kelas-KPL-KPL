﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPL.Shape
{
    class Text : DrawingObject
    {
        public string Value { get; set; }
        public Point startPoint { get; set; }
        public Point finishPoint { get; set; }
        private Brush myBrush;
        private Font font;
        private Pen pen;
        private SizeF textsize;
        public Text()
        {
            this.pen = new Pen(Color.Black);
        }
        public Text(Point initX) : this()
        {
            this.startPoint = initX;
            font = new Font("Arial",16,FontStyle.Italic,GraphicsUnit.Pixel);
        }

        public Text(Point initX, Point initY) : this(initX)
        {
            this.finishPoint = initY;
        }
        public override void DrawEdit()
        {
            pen.Color = Color.Blue;
            pen.DashStyle = DashStyle.DashDotDot;
            if (Value.Equals("Tulis Sesuatu"))
            {
                Form2 form2 = new Form2();
                form2.ShowDialog();
                Value = form2.getTest();
            }
            myBrush = new SolidBrush(System.Drawing.Color.Black);
            this.graphics.DrawRectangle(pen, startPoint.X, startPoint.Y, textsize.Width, textsize.Height);
            this.graphics.DrawString(Value, font, myBrush, startPoint.X, startPoint.Y);           
        }

        public override void DrawIdle()
        {
            myBrush = new SolidBrush(System.Drawing.Color.Black);
            this.graphics.DrawString(Value, font, myBrush, startPoint.X, startPoint.Y);
            textsize = this.graphics.MeasureString(Value, font);
        }

        public override void DrawPreview()
        {
            myBrush = new SolidBrush(System.Drawing.Color.Gray);
            this.graphics.DrawString(Value,font,myBrush,startPoint.X,startPoint.Y);
            textsize = this.graphics.MeasureString(Value, font);
        }

        public override bool HitArea(int x, int y)
        {
            if ((x >= startPoint.X && x <= startPoint.X + textsize.Width) && (y >= startPoint.Y && y <= startPoint.Y + textsize.Height))
            {
                return true;
            }
            return false;
        }

        public override Point GetCenterPoint()
        {
            throw new NotImplementedException();
        }
        public override Point InLeft()
        {
            throw new NotImplementedException();
        }

        public override Point InRight()
        {
            throw new NotImplementedException();
        }

        public override Point InBot()
        {
            throw new NotImplementedException();
        }

        public override Point FromRight()
        {
            throw new NotImplementedException();
        }

        public override Point FormLeft()
        {
            throw new NotImplementedException();
        }

        public override Point FromTop()
        {
            throw new NotImplementedException();
        }

        public override string Type()
        {
            String nama = "Text";
            return nama;
        }

        public override void ChangeValue()
        {
            Form2 form2 = new Form2();
            form2.ShowDialog();
            Value = form2.getTest();
        }

        public override void Move(MouseEventArgs e, int x, int y)
        {
            this.startPoint = new Point((this.startPoint.X + x), (this.startPoint.Y + y));

        }
    }
}
