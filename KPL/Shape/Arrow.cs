﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPL.Shape
{
    class Arrow : DrawingObject, IObserver
    {
        private const double EPSILON = 3.0;
        public Point startPoint;
        public Point finishPoint;
        private Pen pen;
        public DrawingObject objectSource;
        public DrawingObject objectDestination;


        public Arrow(DrawingObject objectSource, DrawingObject objectDestination)
        {
            this.pen = new Pen(Color.Black);
            this.objectSource = objectSource;
            this.objectDestination = objectDestination;
            Update();
        }
        public override void DrawEdit()
        {
            Pen p = new Pen(Color.Blue, 1);
            Pen p2 = new Pen(p.Color, 12);

            this.graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            p2.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            int x1 = startPoint.X;
            int y1 = startPoint.Y;
            int x2 = finishPoint.X;
            int y2 = finishPoint.Y;
            double angle = Math.Atan2(y2 - y1, x2 - x1);

            this.graphics.DrawLine(p, x1, y1, x2, y2);
            this.graphics.DrawLine(p2, x2, y2, x2 + (float)(Math.Cos(angle)), y2 + (float)(Math.Sin(angle)));
        }

        public override bool HitArea(int x, int y)
        {
            double a = (double)(finishPoint.Y - startPoint.Y) / (double)(finishPoint.X - startPoint.X);
            double b = finishPoint.Y - a * finishPoint.X;
            double c = a * x + b;

            if (Math.Abs(y - c) < EPSILON)
            {
                return true;
            }
            return false;
        }

        public override void DrawIdle()
        {
            Pen p = new Pen(Color.Black, 1);
            Pen p2 = new Pen(p.Color, 12);

            this.graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            p2.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            int x1 = startPoint.X;
            int y1 = startPoint.Y;
            int x2 = finishPoint.X;
            int y2 = finishPoint.Y;
            double angle = Math.Atan2(y2 - y1, x2 - x1);

            this.graphics.DrawLine(p, x1, y1, x2, y2);
            this.graphics.DrawLine(p2, x2, y2, x2 + (float)(Math.Cos(angle)), y2 + (float)(Math.Sin(angle)));
        }
        public override void DrawPreview()
        {
            Pen p = new Pen(Color.Blue, 1);
            Pen p2 = new Pen(p.Color, 12);

            this.graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            p2.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            int x1 = startPoint.X;
            int y1 = startPoint.Y;
            int x2 = finishPoint.X;
            int y2 = finishPoint.Y;
            double angle = Math.Atan2(y2 - y1, x2 - x1);

            this.graphics.DrawLine(p, x1, y1, x2, y2);
            this.graphics.DrawLine(p2, x2, y2, x2 + (float)(Math.Cos(angle)), y2 + (float)(Math.Sin(angle)));
        }

        public void Update()
        {
            Point posStart = objectSource.GetCenterPoint();
            Point posFinish = objectDestination.GetCenterPoint();
            Debug.WriteLine("True0");
            Debug.WriteLine("1 ="+posStart.X);
            Debug.WriteLine("2 =" + posFinish.X);
           
            if (posStart.X==posFinish.X)
            {
                this.startPoint = objectSource.FromTop();
                this.finishPoint = objectDestination.InBot();
            }
            else if (finishPoint.Y==startPoint.Y|| finishPoint.Y < posStart.Y + 50 && finishPoint.Y > posStart.Y - 50)
            {
                if (posStart.X <= posFinish.X)
                {
                    Debug.WriteLine("True1");
                    if (posStart.Y < posFinish.Y)
                    {
                        this.startPoint = objectSource.FromRight();
                        this.finishPoint = objectDestination.InLeft();
                    }
                }
                else if (posStart.X >= posStart.Y)
                {
                    if (posStart.Y < posFinish.Y)
                    {
                        this.startPoint = objectSource.FormLeft();
                        this.finishPoint = objectDestination.InRight();
                    }
                }
            }
            else if (finishPoint.X < posStart.X + 100 && finishPoint.X > posStart.X - 100)
            {
                this.startPoint = objectSource.FromTop();
                this.finishPoint = objectDestination.InBot();
            }
            else if (posStart.X<=posFinish.X)
            {
                Debug.WriteLine("True1");
                if (posStart.Y<posFinish.Y)
                {
                    this.startPoint = objectSource.FromRight();
                    this.finishPoint = objectDestination.InLeft();
                }
            }
            else if (posStart.X>=posStart.Y)
            {
                if (posStart.Y < posFinish.Y)
                {
                    this.startPoint = objectSource.FormLeft();
                    this.finishPoint = objectDestination.InRight();
                }
            }
        }

        public override Point GetCenterPoint()
        {
            throw new NotImplementedException();
        }

        public override Point InLeft()
        {
            throw new NotImplementedException();
        }

        public override Point InRight()
        {
            throw new NotImplementedException();
        }

        public override Point InBot()
        {
            throw new NotImplementedException();
        }

        public override Point FromRight()
        {
            throw new NotImplementedException();
        }

        public override Point FormLeft()
        {
            throw new NotImplementedException();
        }

        public override Point FromTop()
        {
            throw new NotImplementedException();
        }

        public override string Type()
        {
            String nama = "Arrow";
            return nama;
        }

        public override void ChangeValue()
        {
            throw new NotImplementedException();
        }

        public override void Move(MouseEventArgs e, int x, int y)
        {
            this.startPoint = new Point((this.startPoint.X + x), (this.startPoint.Y + y));
            this.finishPoint = new Point((this.finishPoint.X + x), (this.finishPoint.Y + y));
        }
    }
}
