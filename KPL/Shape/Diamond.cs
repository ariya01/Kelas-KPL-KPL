﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPL.Shape
{
    class Diamond : DrawingObject
    {
        private const double EPSILON = 3.0;
        public Point startPoint { get; set; }
        public Point finishPoint { get; set; }
        private Pen pen;
       
        public Diamond()
        {
            this.pen = new Pen(Color.Black);
        }

        public Diamond(Point initX):this()
        {
            this.startPoint = initX;
        }

        public Diamond(Point initX,Point initY):this(initX)
        {
            this.finishPoint = initY;
        }
         

        public override void DrawEdit()
        {
            pen.Color = Color.Blue;
            pen.DashStyle = DashStyle.Solid;
            Point point1 = new Point((finishPoint.X - startPoint.X) / 2 + startPoint.X, startPoint.Y);
            Point point2 = new Point(startPoint.X, (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y);
            Point point3 = new Point((finishPoint.X - startPoint.X) / 2 + startPoint.X, finishPoint.Y);
            Point point4 = new Point(finishPoint.X, (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y);
            Point[] titik = { point1, point2, point3, point4 };
            this.graphics.DrawPolygon(pen, titik);
            Notify();
        }

        public override bool HitArea(int x, int y)
        {
            if ((x >= startPoint.X && x <= startPoint.X + finishPoint.X) && (y >= startPoint.Y && y <= startPoint.Y + finishPoint.Y))
            {
                return true;
            }
            return false;
        }

        public override void DrawIdle()
        {
            pen.Color = Color.Black;
            pen.DashStyle = DashStyle.Solid;
            Point point1 = new Point((finishPoint.X - startPoint.X) / 2 + startPoint.X, startPoint.Y);
            Point point2 = new Point(startPoint.X, (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y);
            Point point3 = new Point((finishPoint.X - startPoint.X) / 2 + startPoint.X, finishPoint.Y);
            Point point4 = new Point(finishPoint.X, (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y);
            Point[] titik = { point1, point2, point3, point4 };
            this.graphics.DrawPolygon(pen, titik);
            Notify();
        }
        public override void DrawPreview()
        {
            pen.Color = Color.Blue;
            pen.DashStyle = DashStyle.DashDotDot;
            Point point1 = new Point((finishPoint.X-startPoint.X)/2+startPoint.X,startPoint.Y);
            Point point2 = new Point(startPoint.X, (finishPoint.Y-startPoint.Y)/2+startPoint.Y);
            Point point3 = new Point((finishPoint.X - startPoint.X) / 2 + startPoint.X, finishPoint.Y);
            Point point4 = new Point(finishPoint.X, (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y);
            Point[] titik = { point3,point2,point1,point4};
            this.graphics.DrawPolygon(pen, titik);
            Notify();
        }

        public override Point GetCenterPoint()
        {
            Point point = new Point();
            point.X = ((finishPoint.X - startPoint.X) / 2 + startPoint.X);
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y;
            return point;
        }

        public override Point InLeft()
        {
            Point point = new Point();
            point.X = ((finishPoint.X - startPoint.X) / 2 + startPoint.X- ((finishPoint.X - startPoint.X) / 2));
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y;
            return point;
        }

        public override Point InRight()
        {
            Point point = new Point();
            point.X = ((finishPoint.X - startPoint.X) / 2 + startPoint.X + ((finishPoint.X - startPoint.X) / 2));
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y;
            return point;
        }

        public override Point InBot()
        {
            Point point = new Point();
            point.X = ((finishPoint.X - startPoint.X) / 2 + startPoint.X);
            point.Y = ((finishPoint.Y - startPoint.Y) / 2 + startPoint.Y) - ((finishPoint.Y - startPoint.Y) / 2);
            return point;
        }

        public override Point FromRight()
        {
            Point point = new Point();
            point.X = ((finishPoint.X - startPoint.X) / 2 + startPoint.X)+((finishPoint.X-startPoint.X)/2);
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y;
            return point;
        }

        public override Point FormLeft()
        {
            Point point = new Point();
            point.X = ((finishPoint.X - startPoint.X) / 2 + startPoint.X) - ((finishPoint.X - startPoint.X) / 2);
            point.Y = (finishPoint.Y - startPoint.Y) / 2 + startPoint.Y;
            return point;
        }

        public override Point FromTop()
        {
            Point point = new Point();
            point.X = ((finishPoint.X - startPoint.X) / 2 + startPoint.X) ;
            point.Y = ((finishPoint.Y - startPoint.Y) / 2 + startPoint.Y) + ((finishPoint.Y - startPoint.Y) / 2);
            return point;
        }

        public override string Type()
        {
            String nama = "Diamond";
            return nama;
        }

        public override void ChangeValue()
        {
            throw new NotImplementedException();
        }

        public override void Move(MouseEventArgs e, int x, int y)
        {
            this.startPoint = new Point((this.startPoint.X + x), (this.startPoint.Y + y));
            this.finishPoint = new Point((this.finishPoint.X + x), (this.finishPoint.Y + y));
        }
    }
}
