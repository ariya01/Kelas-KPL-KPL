﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPL
{
    public partial class Form2 : Form
    {
        TextBox objTextBox;
        string theText;
        public Form2()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            objTextBox = (TextBox)textBox1;
            theText = objTextBox.Text;
        }

        public string getTest()
        {
            return theText;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void close()
        {
            this.Close();
        }
    }
}
