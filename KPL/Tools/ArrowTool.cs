﻿using KPL.Shape;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPL.Tools
{
    class ArrowTool : ToolStripButton, Tool
    {
        private DrawingCanvas drawingCanvas;
        Point point;
        public DrawingObject objectSource;
        public DrawingObject objectDestination;
        public Cursor Cursor
        {
            get
            {
                return Cursors.Arrow;
            }
        }

        public DrawingCanvas TargetCanvas
        {
            get
            {
                return this.drawingCanvas;
            }

            set
            {
                this.drawingCanvas = value;
            }

        }

        public ArrowTool()
        {
            this.Name = "Arrow Tool";
            this.ToolTipText = "Arrow Tool";
            Debug.WriteLine(this.Name + "is initialized.");
            Init();
        }

        public void Init()
        {
            this.Image = IconSet.arrow;
            this.CheckOnClick = true;
        }

        public void ToolMouseDown(object sender, MouseEventArgs e)
        {
            point = e.Location;

            /*if (e.Button == MouseButtons.Left)
            {
                line = new Arrow(new Point(e.X, e.Y));
                line.finishPoint = new Point(e.X, e.Y);
                drawingCanvas.AddDrawingObject(line);
            }*/

            if (e.Button == MouseButtons.Left && drawingCanvas != null)
            {
                point = e.Location;

                if (e.Button == MouseButtons.Left && drawingCanvas != null)
                {
                    drawingCanvas.DeselectAll();
                    objectSource = drawingCanvas.SelectObject(e.X, e.Y);

                }
            }
        }

        public void ToolMouseUp(object sender, MouseEventArgs e)
        {
            /*if (e.Button == MouseButtons.Left)
            {
                line.finishPoint = new Point(e.X, e.Y);
                line.Selected();
            }*/
            if (e.Button == MouseButtons.Left && drawingCanvas != null)
            {
                drawingCanvas.DeselectAll();

                if (objectSource != null)
                {
                    objectDestination = drawingCanvas.SelectObject(e.X, e.Y);

                    Arrow connector = new Arrow(objectSource, objectDestination);
                    objectSource.Attach(connector);
                    objectDestination.Attach(connector);

                    drawingCanvas.AddDrawingObjectToFront(connector);
                    connector.ChangeState(IdleState.GetInstance());

                }
            }
        }

        public void ToolMouseMove(object sender, MouseEventArgs e)
        {
            
        }

        public void ToolMouseDoubleClick(object sender, MouseEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void ToolKeyDown(object sender, KeyEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void ToolKeyUp(object sender, KeyEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
